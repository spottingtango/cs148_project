# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.2

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/cs148/cs148/project/LearnOpenGL

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/cs148/cs148/project/LearnOpenGL/build

# Include any dependencies generated for this target.
include CMakeFiles/3.2.blending_sort.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/3.2.blending_sort.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/3.2.blending_sort.dir/flags.make

CMakeFiles/3.2.blending_sort.dir/src/4.advanced_opengl/3.2.blending_sort/blending_sorted.cpp.o: CMakeFiles/3.2.blending_sort.dir/flags.make
CMakeFiles/3.2.blending_sort.dir/src/4.advanced_opengl/3.2.blending_sort/blending_sorted.cpp.o: ../src/4.advanced_opengl/3.2.blending_sort/blending_sorted.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/cs148/cs148/project/LearnOpenGL/build/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/3.2.blending_sort.dir/src/4.advanced_opengl/3.2.blending_sort/blending_sorted.cpp.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/3.2.blending_sort.dir/src/4.advanced_opengl/3.2.blending_sort/blending_sorted.cpp.o -c /home/cs148/cs148/project/LearnOpenGL/src/4.advanced_opengl/3.2.blending_sort/blending_sorted.cpp

CMakeFiles/3.2.blending_sort.dir/src/4.advanced_opengl/3.2.blending_sort/blending_sorted.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/3.2.blending_sort.dir/src/4.advanced_opengl/3.2.blending_sort/blending_sorted.cpp.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/cs148/cs148/project/LearnOpenGL/src/4.advanced_opengl/3.2.blending_sort/blending_sorted.cpp > CMakeFiles/3.2.blending_sort.dir/src/4.advanced_opengl/3.2.blending_sort/blending_sorted.cpp.i

CMakeFiles/3.2.blending_sort.dir/src/4.advanced_opengl/3.2.blending_sort/blending_sorted.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/3.2.blending_sort.dir/src/4.advanced_opengl/3.2.blending_sort/blending_sorted.cpp.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/cs148/cs148/project/LearnOpenGL/src/4.advanced_opengl/3.2.blending_sort/blending_sorted.cpp -o CMakeFiles/3.2.blending_sort.dir/src/4.advanced_opengl/3.2.blending_sort/blending_sorted.cpp.s

CMakeFiles/3.2.blending_sort.dir/src/4.advanced_opengl/3.2.blending_sort/blending_sorted.cpp.o.requires:
.PHONY : CMakeFiles/3.2.blending_sort.dir/src/4.advanced_opengl/3.2.blending_sort/blending_sorted.cpp.o.requires

CMakeFiles/3.2.blending_sort.dir/src/4.advanced_opengl/3.2.blending_sort/blending_sorted.cpp.o.provides: CMakeFiles/3.2.blending_sort.dir/src/4.advanced_opengl/3.2.blending_sort/blending_sorted.cpp.o.requires
	$(MAKE) -f CMakeFiles/3.2.blending_sort.dir/build.make CMakeFiles/3.2.blending_sort.dir/src/4.advanced_opengl/3.2.blending_sort/blending_sorted.cpp.o.provides.build
.PHONY : CMakeFiles/3.2.blending_sort.dir/src/4.advanced_opengl/3.2.blending_sort/blending_sorted.cpp.o.provides

CMakeFiles/3.2.blending_sort.dir/src/4.advanced_opengl/3.2.blending_sort/blending_sorted.cpp.o.provides.build: CMakeFiles/3.2.blending_sort.dir/src/4.advanced_opengl/3.2.blending_sort/blending_sorted.cpp.o

# Object files for target 3.2.blending_sort
3_2_blending_sort_OBJECTS = \
"CMakeFiles/3.2.blending_sort.dir/src/4.advanced_opengl/3.2.blending_sort/blending_sorted.cpp.o"

# External object files for target 3.2.blending_sort
3_2_blending_sort_EXTERNAL_OBJECTS =

bin/4.advanced_opengl/3.2.blending_sort: CMakeFiles/3.2.blending_sort.dir/src/4.advanced_opengl/3.2.blending_sort/blending_sorted.cpp.o
bin/4.advanced_opengl/3.2.blending_sort: CMakeFiles/3.2.blending_sort.dir/build.make
bin/4.advanced_opengl/3.2.blending_sort: /usr/local/lib/libglfw3.a
bin/4.advanced_opengl/3.2.blending_sort: CMakeFiles/3.2.blending_sort.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX executable bin/4.advanced_opengl/3.2.blending_sort"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/3.2.blending_sort.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/3.2.blending_sort.dir/build: bin/4.advanced_opengl/3.2.blending_sort
.PHONY : CMakeFiles/3.2.blending_sort.dir/build

CMakeFiles/3.2.blending_sort.dir/requires: CMakeFiles/3.2.blending_sort.dir/src/4.advanced_opengl/3.2.blending_sort/blending_sorted.cpp.o.requires
.PHONY : CMakeFiles/3.2.blending_sort.dir/requires

CMakeFiles/3.2.blending_sort.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/3.2.blending_sort.dir/cmake_clean.cmake
.PHONY : CMakeFiles/3.2.blending_sort.dir/clean

CMakeFiles/3.2.blending_sort.dir/depend:
	cd /home/cs148/cs148/project/LearnOpenGL/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/cs148/cs148/project/LearnOpenGL /home/cs148/cs148/project/LearnOpenGL /home/cs148/cs148/project/LearnOpenGL/build /home/cs148/cs148/project/LearnOpenGL/build /home/cs148/cs148/project/LearnOpenGL/build/CMakeFiles/3.2.blending_sort.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/3.2.blending_sort.dir/depend

