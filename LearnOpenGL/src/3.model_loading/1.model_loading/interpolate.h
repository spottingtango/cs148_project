#ifndef INTERPOLATE_H
#define INTERPOLATE_H

double LinearInterpolation(vector<double> *x,vector<double> *y, double xq){
    double yq;
    int N = x->size();
    
    if(xq <= x->at(0))
        yq = y->at(0);
    else if(xq >= x->at(N-1))
        yq = y->at(N-1);
    else{
        for(int i=0;i<x->size();i++){
            if( x->at(i) <= xq && xq <= x->at(i+1) ){
                double u = (xq-x->at(i)) / (x->at(i+1)-x->at(i));                              // Normalized interpolation value u in [0,1]
                yq = (1-u)*y->at(i) + u*y->at(i+1);
                //printf("L = %e DC = %8.4f\n",xq,yq);
                return yq;
            }
        }
    }
    //%printf("L = %e DC = %8.4f\n",xq,yq);
    return yq;
}

vector<double> QuaternionNormalize(vector<double> q){
    double norm = 0;
    for(int i=0;i<4;i++){
        norm += q[i]*q[i];
    }
    norm = pow(norm,0.5);
    for(int i=0;i<4;i++){
        q[i] = q[i] / norm;
    }
    return q;
}

vector<double> QuaternionSlerp(vector<double> q0, vector<double> qf, double u){
    
    vector<double> qm(4);
    
    // error message
    if (u<0 || u>1)
        cout << "ERROR: U must be in [0,1]\n";
    
    // normalize
    q0 = QuaternionNormalize(q0);
    qf = QuaternionNormalize(qf);
    
    // negate qf if necessary
    double dp = q0[0]*qf[0] + q0[1]*qf[1] + q0[2]*qf[2] + q0[3]*qf[3];
    cout << "dp = " << dp << endl;
	if( dp < 0 ){
        for(int i=0;i<4;i++){
            qf[i] = -1.0*qf[i];
        }
    }
    
    // compute dot product and angle between quaternions
    double theta = acos(dp);
    if (theta <= 1e-15){
        for(int i=0;i<4;i++){
            qm[i] = (1-u)*q0[i] + u*qf[i];
        }
    }else{
        for(int i=0;i<4;i++){
            qm[i] = q0[i]*(sin((1-u)*theta))/sin(theta) + qf[i]*sin(u*theta)/sin(theta);
        }
    }
    
    qm = QuaternionNormalize(qm);
    return qm;
}

#endif
