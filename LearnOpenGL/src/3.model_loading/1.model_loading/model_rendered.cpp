// Std. Includes
#include <string>

// GLEW
#define GLEW_STATIC
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>

// GL includes
#include <learnopengl/shader.h>
#include <learnopengl/camera.h>
#include <learnopengl/model.h>

// GLM Mathemtics
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Other Libs
#include <SOIL.h>
#include <learnopengl/filesystem.h>

// for csv
#include "csv.h"

float scale = 4*0.00011936; 

// Properties
GLuint screenWidth = 1920, screenHeight = 1080;

// Light attributes
glm::vec3 lightPos(40.0f, -1.0f, 90.0f);
glm::vec4 lightPos_(lightPos,1.0f);
float lightAng = 0.0f;

// Function prototypes
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void Do_Movement();

// Camera
//Camera camera(glm::vec3(0.0f, -1.75f, 2.0f));
Camera camera(glm::vec3(0.0f, 0.0f, 26.0f));
bool keys[1024];
GLfloat lastX = 400, lastY = 300;
bool firstMouse = true;

GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;
GLfloat ANIMATION_DELTA = 0.05f;

// First RTN position
int counter = 1;

// Earth angle
float earthAng = 0.0f;

// Earth Scale
float earthScale = 33.0f;

// Earth Pos
glm::vec3 pos(-40.0f, -80.0f, -100.0f);

// Earth initial axis
glm::vec3 earthAxis(0.0f,0.0f,1.0f);

// Environment defines
float THETA = 80.0f * (3.14/180.0f);

// Set scenarios
bool scene1 = true;
bool scene2 = false;


// The MAIN function, from here we start our application and run our Game loop
int main()
{
    // Init GLFW
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    glfwWindowHint(GLFW_SAMPLES, 16);

    GLFWwindow* window = glfwCreateWindow(screenWidth, screenHeight, "Spotting Tango", nullptr, nullptr); // Windowed
    glfwMakeContextCurrent(window);

    // Set the required callback functions
    glfwSetKeyCallback(window, key_callback);

    // Options
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    // Initialize GLEW to setup the OpenGL Function pointers
    glewExperimental = GL_TRUE;
    glewInit();

    // Define the viewport dimensions
    glViewport(0, 0, screenWidth, screenHeight);

    // Setup some OpenGL options
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    // glEnable(GL_FRAMEBUFFER_SRGB); 
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_MULTISAMPLE);  

    // Setup and compile our shaders
    Shader shader_tango("/home/cs148/cs148/project/LearnOpenGL/src/3.model_loading/1.model_loading/shader.vs", "/home/cs148/cs148/project/LearnOpenGL/src/3.model_loading/1.model_loading/shader.frag");
    Shader planetShader("/home/cs148/cs148/project/LearnOpenGL/src/3.model_loading/1.model_loading/planet.vs", "/home/cs148/cs148/project/LearnOpenGL/src/3.model_loading/1.model_loading/planet.frag");
    Shader atmShader("/home/cs148/cs148/project/LearnOpenGL/src/3.model_loading/1.model_loading/atm.vs", "/home/cs148/cs148/project/LearnOpenGL/src/3.model_loading/1.model_loading/atm.frag");
    Shader nightShader("/home/cs148/cs148/project/LearnOpenGL/src/3.model_loading/1.model_loading/night.vs", "/home/cs148/cs148/project/LearnOpenGL/src/3.model_loading/1.model_loading/night.frag");
    Shader starShader("/home/cs148/cs148/project/LearnOpenGL/src/3.model_loading/1.model_loading/star.vs", "/home/cs148/cs148/project/LearnOpenGL/src/3.model_loading/1.model_loading/star.frag");

    // Load models
    Model ourModel(FileSystem::getPath("resources/objects/tango/tango.obj").c_str());
    Model planet(FileSystem::getPath("resources/objects/planet/planet.obj").c_str());
    Model star(FileSystem::getPath("resources/objects/star/star.obj").c_str());
    Model atmosphere(FileSystem::getPath("resources/objects/planet/planet.obj").c_str());
    Model night(FileSystem::getPath("resources/objects/planet/atm.obj").c_str());

    // Draw in wireframe
    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    GLuint lightVAO;
    glGenVertexArrays(1, &lightVAO);
    glBindVertexArray(lightVAO);

    // Game loop
    while(!glfwWindowShouldClose(window))
    {
        // Set frame time
        GLfloat currentTime = glfwGetTime();

        if (currentTime - lastFrame >= ANIMATION_DELTA){
            // Check and call events
            glfwPollEvents();
            Do_Movement();

            // Clear the colorbuffer
            glClearColor(0.05f, 0.05f, 0.05f, 1.0f);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            // Update light position
            glm::mat4 lightPosMat;
            lightPosMat = glm::rotate(lightPosMat,glm::radians(lightAng),glm::vec3(0.0,1.0,0.0)); 
            lightAng = lightAng - 1.2f;
            glm::vec4 lightPosTemp = lightPosMat * lightPos_;
            lightPos.x = lightPosTemp[0];
            lightPos.y = lightPosTemp[1];
            lightPos.z = lightPosTemp[2];


            // Transformation matrices
            if (camera.Position[2] > 0.0){
                camera.Position[2] = camera.Position[2] - 0.05;
            }

            cout<<"camera is at "<<camera.Position[2]<<endl;
            glm::mat4 projection = glm::perspective(camera.Zoom, (float)screenWidth/(float)screenHeight, 0.1f, 100.0f);
            glm::mat4 view = camera.GetViewMatrix();

            shader_tango.Use();
            GLint lightColorLoc  = glGetUniformLocation(planetShader.Program, "lightColor");
            GLint lightPosLoc    = glGetUniformLocation(planetShader.Program, "lightPos");  
            GLint viewPosLoc     = glGetUniformLocation(planetShader.Program, "viewPos");
            glUniform3f(lightColorLoc,  1.0f, 1.0f, 1.0f);
            glUniform3f(lightPosLoc,    lightPos.x, lightPos.y, lightPos.z);
            glUniform3f(viewPosLoc,     camera.Position.x, camera.Position.y, camera.Position.z);   
            glUniformMatrix4fv(glGetUniformLocation(shader_tango.Program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
            glUniformMatrix4fv(glGetUniformLocation(shader_tango.Program, "view"), 1, GL_FALSE, glm::value_ptr(view));

            planetShader.Use(); 
            lightColorLoc  = glGetUniformLocation(planetShader.Program, "lightColor");
            lightPosLoc    = glGetUniformLocation(planetShader.Program, "lightPos");  
            viewPosLoc     = glGetUniformLocation(planetShader.Program, "viewPos");
            glUniform3f(lightColorLoc,  1.0f, 1.0f, 1.0f);
            glUniform3f(lightPosLoc,    lightPos.x, lightPos.y, lightPos.z);
            glUniform3f(viewPosLoc,     camera.Position.x, camera.Position.y, camera.Position.z);
            glUniformMatrix4fv(glGetUniformLocation(planetShader.Program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));            
            glUniformMatrix4fv(glGetUniformLocation(planetShader.Program, "view"), 1, GL_FALSE, glm::value_ptr(view));    

            atmShader.Use(); 
            lightColorLoc  = glGetUniformLocation(atmShader.Program, "lightColor");
            lightPosLoc    = glGetUniformLocation(atmShader.Program, "lightPos");  
            viewPosLoc     = glGetUniformLocation(atmShader.Program, "viewPos");
            GLint atm_objColor       = glGetUniformLocation(starShader.Program, "objectColor");
            glUniform3f(atm_objColor,       0.5f, 0.5f, 1.0f);   
            glUniform3f(lightColorLoc,  1.0f, 1.0f, 1.0f);
            glUniform3f(lightPosLoc,    lightPos.x, lightPos.y, lightPos.z);
            glUniform3f(viewPosLoc,     camera.Position.x, camera.Position.y, camera.Position.z);
            glUniformMatrix4fv(glGetUniformLocation(atmShader.Program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));            
            glUniformMatrix4fv(glGetUniformLocation(atmShader.Program, "view"), 1, GL_FALSE, glm::value_ptr(view));   

            nightShader.Use();
            lightColorLoc  = glGetUniformLocation(nightShader.Program, "lightColor");
            lightPosLoc    = glGetUniformLocation(nightShader.Program, "lightPos");  
            viewPosLoc     = glGetUniformLocation(nightShader.Program, "viewPos");
            atm_objColor       = glGetUniformLocation(starShader.Program, "objectColor");
            glUniform3f(atm_objColor,       0.5f, 0.5f, 1.0f);   
            glUniform3f(lightColorLoc,  1.0f, 1.0f, 1.0f);
            glUniform3f(lightPosLoc,    lightPos.x, lightPos.y, lightPos.z);
            glUniform3f(viewPosLoc,     camera.Position.x, camera.Position.y, camera.Position.z);
            glUniformMatrix4fv(glGetUniformLocation(nightShader.Program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));            
            glUniformMatrix4fv(glGetUniformLocation(nightShader.Program, "view"), 1, GL_FALSE, glm::value_ptr(view));   

            starShader.Use();
            GLint star_lightColorLoc  = glGetUniformLocation(starShader.Program, "lightColor");
            GLint star_lightPosLoc    = glGetUniformLocation(starShader.Program, "lightPos");  
            GLint star_viewPosLoc     = glGetUniformLocation(starShader.Program, "viewPos");
            GLint star_objColor       = glGetUniformLocation(starShader.Program, "objectColor");   
            glUniform3f(star_objColor,       1.0f, 1.0f, 1.0f);
            glUniform3f(star_lightColorLoc,  1.0f, 1.0f, 1.0f);
            glUniform3f(star_lightPosLoc,    lightPos.x, lightPos.y, -50.0f);
            glUniform3f(star_viewPosLoc,     camera.Position.x, camera.Position.y, camera.Position.z);
            glUniformMatrix4fv(glGetUniformLocation(starShader.Program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));            
            glUniformMatrix4fv(glGetUniformLocation(starShader.Program, "view"), 1, GL_FALSE, glm::value_ptr(view));    

            // Draw the loaded model
            glm::mat4 model1;
            glm::mat4 model2;
            glm::mat4 model3;
            glm::mat4 model4;
            glm::mat4 reset;

            // Load orbit from csv
            std::vector<float> x = parseCSV(counter,1);
            counter++;

            glm::vec3 Z = -glm::normalize(glm::vec3(x[0],x[1],x[2]));
            glm::vec3 X = glm::cross(glm::vec3(1.0f,0.0f,0.0f),Z);
            glm::vec3 Y = glm::cross(Z,X);

            glm::mat3 R_RTN2CAM;
            R_RTN2CAM[0] = X;
            R_RTN2CAM[1] = Y;
            R_RTN2CAM[2] = Z;
            R_RTN2CAM = glm::transpose(R_RTN2CAM);
            
            // Draw Planet
            planetShader.Use();
            if (scene1 == true){
                model1 = glm::translate(model1, pos); // Scenario 1
                model4 = glm::translate(model4, pos); // Scenario 1
            }
            else{
                //glm::vec3 translateVec = R_RTN2CAM*glm::vec3(-300.0f,0.0f,0.0f);
                glm::vec3 translateVec = R_RTN2CAM*glm::vec3(-100.0f,0.0f,0.0f);
                model1 = glm::translate(model1, glm::vec3(translateVec[0], translateVec[1], translateVec[2])); // Scenario 2
                cout<<translateVec[0]<<","<<translateVec[1]<<","<<translateVec[2]<<endl;
            }

            float earthScale = 30.0f;
            model1 = glm::rotate(model1,glm::radians(90.0f+earthAng),glm::vec3(0.0,1.0,0.0)); 
            model4 = glm::rotate(model4,glm::radians(90.0f+earthAng),glm::vec3(0.0,1.0,0.0)); 
            earthAng = earthAng - 1.0f;
            float atmScale = earthScale + 0.02*earthScale;
            model1 = glm::scale(model1, glm::vec3(earthScale, earthScale, earthScale));
            model4 = glm::scale(model4, glm::vec3(atmScale, atmScale, atmScale));
            glUniformMatrix4fv(glGetUniformLocation(planetShader.Program, "model"), 1, GL_FALSE, glm::value_ptr(model1));
            planet.Draw(planetShader);

            
            // Draw night
            nightShader.Use();
            float nightScale = 0.983;
            glm::mat4 model5 = glm::scale(model4, glm::vec3(nightScale, nightScale, nightScale));
            glUniformMatrix4fv(glGetUniformLocation(nightShader.Program, "model"), 1, GL_FALSE, glm::value_ptr(model5));
            night.Draw(nightShader);

            // Draw atmosphere
            atmShader.Use();
            glUniformMatrix4fv(glGetUniformLocation(atmShader.Program, "model"), 1, GL_FALSE, glm::value_ptr(model4));
            atmosphere.Draw(atmShader);

            


            // Draw satellite
            std::vector<float> temp = parseCSV(counter,2);
            float eul_ang = temp[1];
            float eul_axis [3] = {temp[2],temp[3],temp[4]};
            shader_tango.Use();

            if (scene1 == true)
                model2 = glm::translate(model2, glm::vec3(-x[2] * scale, (x[0]*sin(THETA) + x[1]*cos(THETA)) * scale, (x[0]*cos(THETA)-x[1]*sin(THETA)) * scale)); // Scenario 1
            else{
                glm::vec3 translateVec = R_RTN2CAM*glm::vec3(x[0]*scale,x[1]*scale,x[2]*scale);            
                model2 = glm::translate(model2, glm::vec3(translateVec[0], translateVec[1], translateVec[2])); // Scenario 2    
            }            
            model2 = glm::rotate(model2,eul_ang,glm::vec3(-eul_axis[2],eul_axis[0],-eul_axis[1]));  // Rotate
            float tangoScale = 0.8;
            model2 = glm::scale(model2, glm::vec3(tangoScale, tangoScale, tangoScale));
            glUniformMatrix4fv(glGetUniformLocation(shader_tango.Program, "model"), 1, GL_FALSE, glm::value_ptr(model2));
            ourModel.Draw(shader_tango);

            // Draw stars
            float starScale = 0.15f, star_extend = 75;;
            std::vector<float> star_buffer = parseStarsCSV(counter);
            int n_stars = star_buffer[0];
            cout << "counter = " << counter << ", n_stars = " << n_stars << endl;
            for(int i=0;i<n_stars;i++){
                starShader.Use();
                model3 = reset;
                float star_x,star_y,star_z;
                star_x = star_buffer[4*i+1] * star_extend * 10 ;
                star_y = star_buffer[4*i+2] * star_extend * 10 ;
                star_z = star_buffer[4*i+3] * star_extend;
                model3 = glm::translate(model3, glm::vec3(star_x,star_y,-star_z));
                model3 = glm::scale(model3, glm::vec3(starScale,starScale,starScale));
                glUniformMatrix4fv(glGetUniformLocation(starShader.Program, "model"), 1, GL_FALSE, glm::value_ptr(model3));

                // change star color
                float max_mag = 10; 
                float vis_mag = (max_mag - star_buffer[4*i+4])/max_mag;
                glUniform3f(star_objColor,vis_mag,vis_mag,vis_mag);

                star.Draw(starShader);
                
            }

            // Swap the buffers
            glfwSwapBuffers(window); 

            // update time for last frame
            lastFrame = currentTime;   
        }
        
    }

    glfwTerminate();
    return 0;
}

#pragma region "User input"

// Moves/alters the camera positions based on user input
void Do_Movement()
{
    // Camera controls
    if(keys[GLFW_KEY_W])
        camera.ProcessKeyboard(FORWARD, deltaTime);
    if(keys[GLFW_KEY_S])
        camera.ProcessKeyboard(BACKWARD, deltaTime);
    if(keys[GLFW_KEY_A])
        camera.ProcessKeyboard(LEFT, deltaTime);
    if(keys[GLFW_KEY_D])
        camera.ProcessKeyboard(RIGHT, deltaTime);
}

// Is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
    if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);

    if(action == GLFW_PRESS)
        keys[key] = true;
    else if(action == GLFW_RELEASE)
        keys[key] = false;	
}

// void mouse_callback(GLFWwindow* window, double xpos, double ypos)
// {
//     if(firstMouse)
//     {
//         lastX = xpos;
//         lastY = ypos;
//         firstMouse = false;
//     }

//     GLfloat xoffset = xpos - lastX;
//     GLfloat yoffset = lastY - ypos; 
    
//     lastX = xpos;
//     lastY = ypos;

//     // camera.ProcessMouseMovement(xoffset, yoffset);
// }	

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
    // camera.ProcessMouseScroll(yoffset);
}

#pragma endregion
