#version 330 core
in vec2 TexCoords;
in vec3 FragPos;  
in vec3 Normal;

out vec4 color;

uniform vec3 lightPos; 
uniform vec3 viewPos;
uniform vec3 lightColor;
uniform vec3 objectColor;

uniform sampler2D texture_diffuse1;

void main()
{
	// Ambient light calculation
    vec3 Emissive = 0.05f * lightColor; // ambient light is just light color * strength

    // Diffuse light calculation
    vec3 norm = normalize(Normal); // normalizing the vertex normal to get unit direction vector
    vec3 lightDir = normalize(lightPos - FragPos); // unit direction from light to fragment position
    float diff = max(dot(norm,lightDir),0.0f); // dot product of fragment direction and the light direction, max to take care of negative dot products
    vec3 Diffuse = diff * lightColor; // diffused light is diff (light direction component) times light color

    // Specular light calculation
    vec3 viewDir = normalize(viewPos - FragPos); // unit direction from viewer to fragment
    vec3 reflectDir = reflect(-lightDir,norm); // reflection direction 
    vec3 halfwayDir = normalize(lightDir + viewDir); // blinn
    float spec = pow(max(dot(norm, halfwayDir), 0.0), 64);
    vec3 Specular = 1.0f * spec * lightColor; // specular light component

    // color = texture(texture_diffuse1, TexCoords);
    color = vec4((Emissive + Diffuse + Specular),1.0f) * texture(texture_diffuse1, TexCoords);
    
}