#ifndef CSV_H
#define CSV_H

#include <vector>
#include <string>
#include <fstream>
#include <sstream>

std::vector<float> parseCSV(int n,int flag) {
    int n_line = 1;
    int n_array = 0;
    string fn;
    std::vector<float> x(6);

    switch(flag){
        case 1:
            fn = "/home/cs148/cs148/project/LearnOpenGL/resources/objects/tango/RTNState.csv";
            break;
        case 2:
            fn = "/home/cs148/cs148/project/LearnOpenGL/resources/objects/tango/attitude.csv";
            break;
        default:
            cout << "ERROR: UNKNOWN CSV FILE\n";
    }

    std::ifstream  data(fn);
    std::string line;
    while (std::getline(data, line)) {
        std::stringstream  lineStream(line);
        std::string        cell;
        while (std::getline(lineStream, cell, ',')) {
            x[n_array] = std::stod(cell);
            n_array++;
        }
        if (n == n_line)
            return x;
        n_line++;        n_array = 0;
    }
    return x;
}

std::vector<float> parseStarsCSV(int n) {
    int n_line = 1;
    int n_array = 0;
    string fn = "/home/cs148/cs148/project/LearnOpenGL/resources/objects/tango/stars.csv";;
    n = n% 250;

    std::vector<float> x(161);

    std::ifstream  data(fn);
    std::string line;
    while (std::getline(data, line)) {
        std::stringstream  lineStream(line);
        std::string        cell;
        while (std::getline(lineStream, cell, ',')) {
            x[n_array+1] = std::stod(cell);
            n_array++;
        }
        x[0] = n_array/4;
        if (n == n_line)
            return x;
        n_line++;        
        n_array = 0;
    }
    return x;
}

#endif