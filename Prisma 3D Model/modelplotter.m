function pp = modelplotter(edges,points,mode)

if nargin==2
    mode = 1;%plotting with edges
elseif nargin == 1
    mode = 2;%for plotting only vertices (default option if only one input)
    points = edges;
end

switch mode
    
    case 1
        hold on
        for i = 1:length(edges)
            
                x = [points(edges(i,1),1);points(edges(i,2),1)];
                y = [points(edges(i,1),2);points(edges(i,2),2)];
                z = [points(edges(i,1),3);points(edges(i,2),3)];
                pp = plot3(x,y,z,'r');
            
        end
        hold off
        
    case 2
        pp = plot3(points(:,1),points(:,2),points(:,3),'b*','markers',0.5)  ;

end

end
